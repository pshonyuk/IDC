<h1>
	Нормативи програми
</h1>


	<div class="body">
		<h4>Програма екологічного маркування „Зелена садиба”</h4>
		<h4>Екологічне маркування агросадиб – інструмент сталого розвитку українського села</h4>

		<p>Сталим розвитком називають такий ріст економічних та соціальних показників, який не приводить до виснаження ресурсів, а, навпаки, сприяє їх якісному та кількісному відновленню. З цієї точки зору відпочинок у селі є одним із найбільш перспективних напрямків відродження та розвитку українського села, оскільки не лише не виснажує наявні ресурси, але й робить місцевих жителів економічно зацікавленими у їх збереженні та примноженні. Адже розвиток туристичного бізнесу приносить дохід не лише власникам осель, а й багатьом іншим жителям даної місцевості. Створюються нові робочі місця в сфері обслуговування туристів, зростає попит на продукти харчування, сувенірну продукцію та вироби місцевих майстрів, активізується будівництво та транспортні послуги. Отже, всі зацікавлені у зростанні привабливості своєї місцевості для туристів. Але відповідні механізми та методи господарювання не завжди є очевидними, тож тут є сенс спиратись на рекомендації спеціалістів.</p>

		<p>Відтепер в Україні також запроваджена система екологічного маркування агросадиб знаком “Зелена садиба”. Цей знак може збільшити популярність вашої садиби серед екологічно-свідомих туристів. Знак надається Спілкою сприяння розвитку сільського зеленого туризму в Україні та може використовуватись як маркетинговий інструмент.</p>
		<p>Для власника такого знаку його привабливість забезпечується двома основними факторами: підвищеним попитом з боку туристів та першочерговим просуванням його послуг на ринок Спілкою та державними установами. Перший з цих факторів, тобто підвищення попиту, на сьогоднішній день спрацьовує  головним чином стосовно зарубіжних споживачів, які є бажаними гостями в українських оселях завдяки їх високій культурі та платоспроможності. Але на відміну від європейських туристів, більшість з яких за соціальними опитуваннями надає перевагу відпочинку в екологічно маркованих оселях, авторитет знаку “Зелена садиба” у вітчизняного споживача ще належить заробити. І тут знадобляться спільні зусилля  не тільки держави та громадськості, а й самих власників садиб. Що ж до просування на ринок, то ефективність цього процесу значною мірою залежить від узгодженості екологічного маркування з високим рівнем туристичного сервісу. Лише якісні послуги можуть бути предметом маркетингових зусиль.</p>
		<p>Слід зазначити, що подібні схеми у європейських країнах мають чимало спільних рис, що обумовлено єдиним підходом до екологічного маркування як фактору підтримки та сприяння сталому розвитку. Це підтверджується схожістю основних розділів вимог екологічної сертифікації. До них, як правило, належать: зменшення шкідливого впливу агротуристичного об’єкту на навколишнє середовище, економія ресурсів, підтримка народних традицій та ремесел, підтримка місцевої економіки, розвиток екологічно-сприятливих видів розваг та відпочинку. За такими ж принципами побудована і схема сертифікації для отримання знаку “Зелена садиба”. У перспективі назріває необхідність об’єднання різних європейських систем екомаркування в одну міжнародну, про що говорилося, зокрема, на Міжнародній  конференції з питань сільського туризму в Ризі 2-3 червня 2004 року. Таке об’єднання значно полегшить споживачам та турагенціям  проблему орієнтування у великій кількості різних знаків, а також суттєво підвищить авторитет єдиного знаку на міжнародному рівні. Адже очевидно, що повага до знаку (а, відповідно, і ринковий попит на нього) прямо залежать від стабільності та авторитетності організації, що його запровадила. Логічно, таким чином, що на Україні система екологічного маркування вперше запроваджена найбільш авторитетною організацією в царині сільського туризму – Спілкою сприяння розвитку сільського зеленого туризму в Україні.</p>

		<p>На початковому етапі прикладені зусилля можливо не відразу дадуть помітний економічний (не кажучи вже про екологічний) ефект, тож слід бути далекогляднішими і заглядати у завтрашній день.</p>
		<p>Сьогодні для господарів агротуристичних садиб ефективність вкладання грошей в екомаркування не завжди очевидна – адже процедура екологічної сертифікації недешева, та й оплачувати її потрібно зараз, а чи приведе вона до збільшення потоку туристів – невідомо. Виконання вимог екологічного стандарту також потребує деяких витрат, що веде до зростання собівартості туристичних послуг. Але ж не можна все міряти тільки грошовими одиницями і тільки сьогоднішнім днем. Ми є народ і на нашій землі ще жити нашим нащадкам, тож зусилля, витрачені на збереження довкілля та сталий розвиток, не можна вважати зайвими. Втім, розуміючи економічні складності на початковому етапі запровадження екологічного маркування, Спілка спрямовує свої зусилля на пошук альтернативних джерел фінансування процедури сертифікації. На сьогодні процедуру сертифікації пройшли 35 садиб з 7 областей України, 24 з них  отримали знак “Зелена садиба” 1-го рівня і 3 садиби – 2-го рівня. Це було зроблено безкоштовно для власників.</p>

		<p>Що потрібно, щоб отримати сертифікат екологічної якості «ЗЕЛЕНА САДИБА»?</p>
			<ol>

				<li>Уважно прочитати вимоги щодо надання цього знаку, які затверджені Спілкою сприяння розвитку сільського зеленого туризму в Україні.</li>
				<li>Визначитись, в якій мірі Ваша садиба відповідає згаданим Вимогам та які недоліки слід усунути для досягнення достатньої відповідності.</li>
				<li>Усунути зазначені недоліки.</li>
				<li>Звернутись до Спілки з проханням провести процедуру сертифікації Вашої садиби.</li>
				<li>Продемонструвати інспектору Спілки екологічні переваги Вашої садиби та перевірити правильність заповнення анкети.</li>
			</ol>
		<p>Зверніть увагу і на те, що екологічна сертифікація за програмою «Зелена Садиба» є повністю добровільною.  Вона також є незалежною від сертифікації якості розміщення, адже свідчить лише про екологічно сприятливий спосіб ведення господарства. Часом підвищення комфортності розміщення може навіть суперечити екологічним вимогам, зокрема  з приводу економії електроенергії та води, або застосування засобів побутової хімії. Але ж існує чималий попит на відпочинок саме у «традиційному народному» стилі, тож, можливо, Вам підійде саме цей напрямок розвитку. Пам’ятайте також, що цю землю Ви передасте у спадок своїм нащадкам, тож  варто попіклуватись про те, щоб вони отримали її у якомога кращому стані.</p>

		<p>Зупинимось на тих вимогах, невиконання яких найчастіше стає перешкодою до  отримання знаку. Найчастіше це використання  отрутохімікатів у городництві та садівництві. Попри всі запевнення виробників таких речовин, це є несумісним з поняттям екологічно чистих продуктів харчування, а також завдає значної шкоди біорізноманіттю навколишнього середовища. Тож доведеться робити вибір між підвищеним урожаєм картоплі і шляхом екологічно чистого господарювання під знаком “Зелена садиба”.</p>
		<p>Ще однією вимогою, яка не завжди виконується на сьогоднішній день є наявність довідки з санепідемстанції про відповідність джерела питної води державним стандартам. Зауважимо, що таку довідку все одно доведеться отримати всім, хто офіційно приймає туристів для відпочинку і рекламує свої послуги.</p>
		<p>Також іноді стає проблемою неохайне сміттєзвалище на території садиби, або поруч із нею. І якщо з упорядкуванням власної території у дбайливих господарів питань не виникає, то до звалища по той бік огорожі багато хто ставиться терпляче – мовляв, то не моя проблема. Але ж природа єдина по обидва боки огорожі і не може бути чистої садиби в брудному оточенні. Саме власники агросадиб, які, як правило, є найбільш активними і передовими жителями села, повинні поклопотатися про чисте довкілля.</p>

		<p>Як бачимо, розглянуті перешкоди не є нездоланними, а виконання згаданих умов не потребує великих капіталовкладень. Тож приєднуйтесь до екологічного братства під знаком “Зелена садиба”, що є ознакою високої духовної культури та цивілізованості власника.</p>

		<h4>Що потрібно для подовження дії знаку "Зелена садиба"</h4>

		<p>Для подовження дії знаку «Зелена садиба» на наступний термін власник садиби повинен надати  у Комісію з екологічного маркування садиб наступні документи:</p>
			<ol>

		<li>заповнену анкету «Нормативи програми «Зелена садиба», де будуть відмічені ті вимоги, яким садиба задовольняє, завірену власним підписом.</li>
		<li>короткий (не більше сторінки) опис дій та заходів, що здійснюються господарями садиби для покращення екологічного стану довкілля.</li>
		<li>50 гривень (або квитанцію про їх сплату на рахунок Спілки як добровільного внеску на підтримку програми «Зелена садиба»).</li>
			</ol>
		<p>Для отримання знаку «Зелена садиба» більш високого рівня  садибу має відвідати інспектор Спілки.</p>
		<p>Бажаємо успіхів!</p>
		<p>НОРМАТИВИ ЕКОЛОГІЧНОЇ ЯКОСТІ РОЗМІЩЕННЯ ВІДПОЧИВАЮЧИХ У СЕЛІ ДЛЯ НАДАННЯ ЗНАКУ “ЗЕЛЕНА САДИБА”Знак «ЗЕЛЕНА САДИБА» надається агротуристичним садибам, які відповідають вимогам наведених нижче «НОРМАТИВІВ». Знак має три рівні якості, від першого (найнижчого) до третього (найвищого).</p>
		<p>Для отримання знаку першого рівня садиба  має відповідати всім вимогам, позначеним цифрою “1”.  Для отримання сертифікату другого рівня садиба має  відповідати всім вимогам, позначеним цифрою “1” та 80% вимог, позначених цифрою “2”. Для отримання сертифікату третього рівня садиба має  відповідати всім вимогам, позначеним цифрою “1”,  80% вимог, позначених цифрою “2” та 80% вимог, позначених цифрою “3”.</p>
		<p>Для проведення сертифікації до садиби виїжджатиме інспектор, який перевірить відповідність садиби зазначеним вимогам. Зібрана інспектором інформація буде надана Комісії, призначеній Спілкою, яка винесе остаточне рішення про надання знаку «ЗЕЛЕНА САДИБА». Знак «ЗЕЛЕНА САДИБА» надаватиметься на термін 2 роки, що буде підтверджено відповідним сертифікатом.</p>

		<ol>
			<li>ДОВКІЛЛЯ.</li>


			<ol>
				<p>Довкілля садиби повинно бути екологічно чистим та привабливим. господарі повинні стежити за підтримкою та поліпшенням екологічного стану довкілля, сприяти збереженню ландшафтів та біорізноманіття.</p>
				<li>Садиба розташована в мальовничому місці, поблизу є привабливі елементи ландшафту.	“1”</li>
				<li>Околиці садиби чисті і доглянуті, бур’яни виполоті, нема сміття.	“1”</li>
				<li>Довкілля садиби не має  джерел забруднення (сміттєзвалищ, очисних споруд та ін.).	“2”</li>
				<li>Навколишні місця відпочинку доглянуті та очищені від сміття.	“2”</li>
				<li>Садиба розташована на безпечній відстані від джерел екологічного ризику.	“3”</li>
				<li>Поблизу садиби є природоохоронна територія	“3”</li>
			</ol>

			<li>САДИБА.</li>
			<ol>
			<p>Садиба не повинна бути джерелом забруднення; використання екологічно-агресивних речовин слід максимально обмежувати; оформлення території має носити ознаки підтримки сталого розвитку регіону та його екологічного стану.</p>

				<li>Територія садиби доглянута та не має ознак забруднення. 	 “1”</li>
				<li>Сміття та відходи збираються в спеціально облаштованому місці.     	 “1”</li>
				<li>Не застосовуються хімічні засоби захисту рослин.  	 “1”</li>
				<li>Органічні відходи збираються в компостні ями або згодовуються домашнім тваринам.   	 “2”</li>
				<li>Не використовуються пластикові садові меблі. 	 “2”</li>
				<li>Будівельні матеріали не вміщують азбесту. 	 “2”</li>
				<li>Сміття вивозиться централізовано 	 “2”</li>
				<li>Будинок і садиба оформлені у традиційному для даного регіону стилі  	 “2”</li>
				<li>В оформленні садиби використовуються місцеві види рослин 	 “2”</li>
				<li>Покращені умови для видів місцевої фауни (годівниці, гніздів’я)  	 “2”</li>
				<li>Не застосовуються мінеральні добрива. 	 “3”</li>
				<li>Сміття сортується і вивозиться централізовано. 	 “3”</li>
				<li>При будівництві будинку та оформленні садиби використані лише екологічно чисті природні матеріали. 	 “3”</li>
			</ol>
			<li>КІМНАТИ.</li>
			<ol>
				<li>В оформленні кімнат використовуються елементи традиційного стилю, витвори народних ремесел та мистецтв.    	“1”</li>
				<li>Обладнання кімнат виготовлене з використанням натуральних матеріалів місцевого походження. 	“1”</li>
				<li>Куріння дозволене лише в спеціально визначених місцях.   	“2”</li>
				<li>В обладнанні кімнат не використовуються синтетичні матеріали.	“3”</li>
			</ol>
			<li>ПРОДУКТИ.</li>
				<ol>

			<p>Перевага надається екологічно чистим продуктам місцевого виробництва, приготованим за традиційними технологіями.</p>

				<li>Джерело питної води перевірене відповідною лабораторією, довідка про це  доступна для туристів.	“1”</li>
				<li>Їжа готується з використанням свіжих сезонних фруктів та овочів.	“1”</li>
				<li>Доступне вегетаріанське меню. 	“1”</li>
				<li>Туристам пропонуються продукти лише українського виробництва.  	“2”</li>
				<li>Не використовується одноразовий пластиковий посуд.	“2”</li>
				<li>Не використовуються продукти в індивідуальній упаковці.	“2”</li>
				<li>Туристам пропонуються продукти лише місцевого виробництва.	“3”</li>
				<li>Туристам пропонуються сертифіковані продукти органічного землеробства.	“3”</li>
				</ol>
			<li>РАЦІОНАЛЬНЕ ВИКОРИСТАННЯ ВОДНИХ РЕСУРСІВ.</li>
				<ol>

			<p>Використання води з водогонів має бути якомога економнішим. Надається перевага природним джерелам.</p>

				<li>При наявності водогону встановлені системи місцевої каналізації або обладнані асенізаційні колодязі. 	“1”</li>
				<li>Забруднена вода не потрапляє у довкілля.     	“1”</li>
				<li>При наявності водогону крани та регулятори не течуть.	“1”</li>
				<li>Асенізаційні колодязі ізольовані від грунтових вод.	“2”</li>
				<li>Водогін обладнаний лічильником.	“2”</li>
				<li>Кімнати для гостей обладнані окремим лічильником. 	“3”</li>
				<li>У садибі використовується лише вода з природних джерел.	“3”</li>
				</ol>
			<li>РАЦІОНАЛЬНЕ ВИКОРИСТАННЯ ЕЛЕКТРОЕНЕРГІЇ ТА ПАЛИВА</li>
				<ol>

			<p>Переважна більшість палива та джерел електроенергії належить до невідновлюваних ресурсів, через це їх використання слід зводити до мінімуму.</p>

				<li>У садибі встановлені лічильники для електроенергії та газу. 	“1”</li>
				<li>Електропобутові прилади (холодильники, електронагрівачі) вимикаються, коли немає туристів. 	“1”</li>
				<li>Ведеться розрахунок споживання електроенергії та палива туристами. 	“2”</li>
				<li>Електричні нагрівачі та прилади для сушіння рук не використовуються.	“2”</li>
				<li>Опалення кімнат для туристів регулюється індивідуально.	“2”</li>
				<li>Для освітлення використовуються тільки енергоефективні лампи.	“3”</li>
				<li>Для опалення не використовується паливо з невідновлюваних ресурсів.	“3”</li>
				<li>Використовуються альтернативні джерела електроенергії.	“3”</li>
				</ol>
			<li>ОБМЕЖЕНЕ ВИКОРИСТАННЯ ПОБУТОВИХ ХІМІЧНИХ ЗАСОБІВ.</li>
				<ol>

			<p>Засоби побутової хімії як правило є синтетичними матеріалами, агресивними до природного середовища, їх виробництво також належить до найбільш екологічно шкідливих. Через це їх вживання слід обмежувати.</p>

				<li>Використання побутових хімічних засобів зведене до мінімуму   	“1”</li>
				<li>Миючі та дезинфікуючі засоби не містять хлору та бору. 	“2”</li>
				<li>Синтетичні миючі засоби не використовуються. 	“2”</li>
				<li>Засоби побутової хімії не використовуються.	“3”</li>
				<li>Холодильники та аерозолі, що містять фреон не використовуються. 	“3”</li>
				</ol>
			<li>ТУРИСТИЧНА ІНФОРМАЦІЯ.</li>
				<ol>

			<p>Туристи повинні бути забезпечені екологічно-орієнтованою інформацією, що сприяє збереженню довкілля та сталому розвитку регіону.</p>

				<li>Знак “Зелена садиба” демонструється туристам.  	“1”</li>
				<li>Програма “Зелена садиба” доступна туристам.   	“1”</li>
				<li>У садибі розміщені  “Поради для гостей”,  що стосуються  екологічно-прийнятної поведінки. 	“1”</li>
				<li>Є інформація про місцеві природні, культурні та історичні цінності, охорону довкілля та біорізноманіття. 	“2”</li>
				<li>Туристам доступна карта місцевості.	“2”</li>
				<li>Туристам доступна карта місцевості з позначеними природоохоронними об’єктами та територіями. 	“3”</li>
				<li>Спеціалізована екологічна преса передплачується або купується.	“3”</li>
				<li>Власники садиби мають добрі знання місцевих традицій, добре поінформовані щодо місцевих природоохоронних територій, стежок, пам’яток історії та культури. На вимогу вони можуть виконувати функції гідів.	“3”</li>
				</ol>
			<li>ТРАНСПОРТ.</li>
				<ol>

			<p>Надається перевага громадському та екологічно-чистим видам транспорту.</p>

				<li>Туристам доступна інформація про маршрути та розклад громадського транспорту.	“1”</li>
				<li>Туристи забезпечені транспортними послугами між садибою та найближчою зупинкою громадського транспорту.	“2”</li>
				<li>Є можливість оренди екологічно-чистих транспортних засобів (велосипедів, коней). 	“2”</li>
				<li>Облаштовані місця стоянки для автотуристів. 	“2”</li>
				<li>Пішим, вело- та кінним туристам пропонуються пільгові умови. 	“2”</li>
				<li>Господарі пропонують туристам оренду екологічно-чистих транспортних засобів.	“3”</li>
				</ol>
			<li>РЕКОМЕНДОВАНА ТУРИСТИЧНА ДІЯЛЬНІСТЬ.</li>
				<ol>

			<p>Рекомендована діяльність, яка найменше впливає на довкілля: піші, кінні та велосипедні прогулянки, плавання, сонячні ванни, веслування, віндсерфінг, гірські лижі, збирання грибів та ягід, риболовля, спостереження за птахами та тваринами.</p>

				<li>Господарі  надають туристам інформацію про можливість екологічно-сприятливих видів діяльності та відпочинку.	“1”</li>
				<li>Господарі пропонують туристам оренду спорядження для екологічно-сприятливих видів діяльності та відпочинку. 	“2”</li>
				<li>Господарі  активно сприяють розвитку екологічно-сприятливих видів діяльності та відпочинку у своїй місцевості. 	“3”</li>
			</ol>
			<li>ПІДТРИМКА НАРОДНИХ ТРАДИЦІЙ.</li>
				<ol>

			<p>Підтримується збереження та розвиток традиційних для даної місцевості побуту, ремесел та мистецтв.</p>

				<li>В оформленні будинку і садиби використані елементи традиційного стилю та декору.     	“1”</li>
				<li>Будинок і садиба цілком оформлені в традиційному стилі. 	“2”</li>
				<li>Туристи мають можливість ознайомитись чи придбати вироби народних ремесел та мистецтв. 	“2”</li>
				<li>Розміщення та побут туристів організовано згідно зі старовинним укладом селянського життя. 	“3”</li>
				<li>Господарі володіють традиційними ремеслами чи мистецтвами і можуть запропонувати туристам свої вироби та продемонструвати традиційну технологію.	“3”</li>
				<li>Туристам пропонуються тури та екскурсійні заходи, орієнтовані на підтримку та розвиток місцевих традицій, ремесел та мистецтв. 	“3”</li>
				</ol>
			<li>ДОСВІД РОБОТИ.</li>
				<ol>

					<li>Більше двох років діяльності під знаком “ЗЕЛЕНА САДИБА.	“2”</li>
					<li>Більше чотирьох років діяльності під знаком “ЗЕЛЕНА САДИБА”.	“3”</li>
				</ol>










		</ol>


	</div>