var _ = require("underscore");

module.exports = (function(){
	var templates = {};
	return {
		get: function(name){
			return (name ? templates[name] : _.clone(templates));
		},
		add: function(name, html){
			var key, error,
				tpls = (typeof name === "object" ? name : {name: html});
			for(key in tpls){
				if(tpls.hasOwnProperty(key)){
					try{
						templates[key] = _.template(tpls[key]);
					}catch(error){}
				}
			}
		}
	}
})();