var fs = require("fs"),
	nodePath = require("path");


function errorLog(message) {
	console.log("\x1b[31m" + message + "\x1b[0m");
}


var readDir = {
	getListFiles: function(path){
		var list = [], e;

		if(!path) return list;
		try{
			return fs.readdirSync(path);
		}catch(e){
			errorLog(e);
			return list;
		}
	},

	getFile: function(fileName){
		var file = "", e;
		if(!fileName) return ;
		try{
			file = fs.readFileSync(fileName, "utf8");
		}catch(e){
			errorLog(e);
		}
		return file;
	},

	getFiles: function(path){
		var list = [], files = {}, i, fileName, e;
		if(!path) return list;
		list = this.getListFiles(path);
		if(!list){
			return null;
		}
		for(i = 0; i < list.length; i++){
			fileName = list[i];
			try{
				files[fileName] = fs.readFileSync(nodePath.join(path, fileName), "utf8");
			}catch(e){
				errorLog(e);
			}
		}
		return files;
	}
};

module.exports = readDir;